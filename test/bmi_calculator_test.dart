import 'package:bmi/helper/bmi_calculator.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  BmiCalculator calculator = BmiCalculator(height: 100, weight: 50);

  group("Obese Test", () {
    test("Test Bmi calculator : obese category calculation", () {
      final result = calculator.calculateBmi();
      expect(result, 50);
    });
    test("Test Bmi calculator : obese category text", () {
      final result = calculator.determineBmiCategory();
      expect(result, contains("Obese"));
    });

    test("Test Bmi calculator : obese healt description", () {
      final result = calculator.getHealRiskDescription();
      expect(result, contains("Metabolic Syndrome"));
    });
  });
}
