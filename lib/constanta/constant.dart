import 'package:flutter/material.dart';

const Color primaryColor = Color(0xff0A0E21);
// ignore: non_constant_identifier_names
TextStyle LabelTextStyle =
    const TextStyle(fontSize: 18, color: Colors.white //Color(0xff8d8e98)
        );
// ignore: non_constant_identifier_names
TextStyle NumberTextStyle = const TextStyle(
  fontSize: 60,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

const String underweightSevere = "Underweight (Severe thinnes)";
const String underweightModerate = "Underweight (Moderate thinnes)";
const String normal = "Normal";
const String overweight = "Overweight";
const String underweightMild = "Underweight (Mild thinnes)";
const String obeseI = "Obese (Class I)";
const String obeseII = "Obese (Class II)";
const String obeseIII = "Obese (Class IIII)";
